# Simple NodeJS Webapp

The application uses an environment variable `APPLICATION_INSTANCE`.  
This variable will be used by the application to listen on a specific path (i.e. `/${application_instance}/health`).

## Launch the application:

```bash
export APPLICATION_INSTANCE=example
node src/count-server.js
```


## link docker hub
https://hub.docker.com/repository/docker/florianamiot/simple-nodejs-webapp/general



## Answers



Q1: Define what is a Microservices architecture and how does it differ
from a Monolithic application.


Microservices has a decentralized approach approach to designing and building software applications with small, independent services.
The main difference is that Monolithic is single, tightly integrated unit.

• Q2: Compare Microservices and Monolithic architectures by listing their
respective pros and cons.

Microservices:
Pros: Scalability, Flexibility, Fault Isolation.
Cons: Complexity, Communication Overhead.

Monolithic:
Pros: Simplicity, Local Performance.
Cons: Scalability Challenges, Limited Tech Stack.

• Q3: In a Micoservices architecture, explain how should the application be
split and why.

In a microservices architecture, the application should be split based on business capabilities or services. There is  an encapsulation of its data and logic. This split allows for independent development, deployment.

• Q4: Briefly explain the CAP theorem and its relevance to distributed
systems and Microservices.

The CAP theorem asserts that in a distributed system, you can achieve, at most, two out of three guarantees: Consistency, Availability, and Partition Tolerance. In the realm of microservices and distributed systems:

Consistency: All nodes observe identical data simultaneously.
Availability: Every request elicits a response, but it may not guarantee the most recent information.
Partition Tolerance: The system persists even in the face of network partitions or communication breakdowns.

• Q5: What consequences on the architecture ?

Trade-offs impact architecture, leading to distributed data management and increased complexity.


• Q6: Provide an example of how microservices can enhance scalability in
a cloud environment.


In a cloud-based microservices architecture, each microservice can scale independently according to its unique resource requirements. For instance, if an image processing service demands more computational capacity, it can horizontally scale by adding more instances, without impacting other services. 


• Q7: What is statelessness and why is it important in microservices architecture ?

A stateless microservice is one that does not maintain any information about the state of a client between requests. Each request from a client contains all the information needed to fulfill that request.
It is important for scalability because stateless services are easier to scale horizontally because there is no need to manage and synchronize 


• Q8: What purposes does an API Gateway serve ?

The purposes are routing,authentication, load balancing, rate limiting, request transformation, rogging/Monitoring
API Gateway centralizes access and ensures efficient communication with microservices.
